*** Settings ***
Resource    ../../../main/pages/loginpage.robot
Suite Setup    Precondition
Suite Teardown    Postcondition


*** Variables ***
${INVALID_USERNAME}  demo
${INVALID_PASSWORD}  demo

*** Test Cases ***
Login_Failed
    [Documentation]  Error Message is displayed with login is invalid
    [Tags]  test
    Login To Test Site  ${INVALID_USERNAME}  ${INVALID_PASSWORD}
    Verify If Error Message Is Displayed








*** Keywords ***
Login To Test Site
    [Arguments]  ${username}  ${password}
    Input Username  ${username}
    Input Password  ${password}
    Click Login Button

Verify If Error Message Is Displayed
    ${status}  Run Keyword And Return Status  Wait Until Page Contains  ${LOGIN_ERROR_MESSAGE}  3s  #validates if error message container is displayed
    Run Keyword If  '${status}'=='True'  Validate If Correct Error Message Is Displayed  #if container is displayed, validate if error displayed is correct
    ...  ELSE IF  '${status}'=='False'  Fail  Expected error message is not displayed!  #else if not displayed, test case failed
    ...  ELSE  Fail  Unexpected Error Occured!

Validate If Correct Error Message Is Displayed
    ${error}  Get Login Error Message  #gets the error message and validates
    Should Be Equal  ${error}  ${LOGIN_ERROR_MESSAGE}


