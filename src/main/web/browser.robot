*** Keywords ***
Precondition
    Create Chrome Browser
    Open Browser  https://alpha.patient.noonatest.com/patient/#/login   chrome
    Maximize Browser Window

Postcondition
    Close Browser


Create Chrome Browser
    Set Webdriver Property    webdriver.chrome.driver    src/main/drivers/chromedriver.exe
