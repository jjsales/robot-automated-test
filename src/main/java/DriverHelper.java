/**
 * Created by jsales on 6/15/2017.
 */
public class DriverHelper {
    public static final DriverHelper Default = new DriverHelper();

    public DriverHelper() {
    }

    public void setWebDriverProperty(String binary, String dir) {
        System.setProperty(binary, dir);
    }
}

