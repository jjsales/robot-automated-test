*** Settings ***
Resource  ../config/integration_properties.robot


*** Variables ***
#ELEMENTS
${ID_USERNAME_FIELD}  email
${ID_PASSWORD_FIELD}  pwd
${ID_LOGIN_BUTTON}  email-and-password-next-button
${CONTAINER_LOGIN_ERROR}  document.getElementsByClassName('form-error ng-binding ng-scope')[0]
#CONSTANT
${LOGIN_ERROR_MESSAGE}  Incorrect username or password. If you have forgotten your password, please click Problems logging in -link.


*** Keywords ***
Input Username
    [Arguments]  ${username}
    Wait Until Element Is Clickable  ${ID_USERNAME_FIELD}  5s
    Input Text  ${ID_USERNAME_FIELD}  ${username}

Input Password
    [Arguments]  ${password}
    Input Text  ${ID_PASSWORD_FIELD}  ${password}

Click Login Button
    Click Element  ${ID_LOGIN_BUTTON}

Get Login Error Message
    ${error}  Execute Javascript  return ${CONTAINER_LOGIN_ERROR}.innerText;
    [Return]  ${error}
